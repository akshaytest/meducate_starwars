package in.crazyfingers.starwarschallenge;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.crazyfingers.starwarschallenge.adapter.Adapter;
import in.crazyfingers.starwarschallenge.constants.Contants;
import in.crazyfingers.starwarschallenge.constants.Util;
import in.crazyfingers.starwarschallenge.custom.RecyclerItemClickListener;
import in.crazyfingers.starwarschallenge.moviedetails.ActivityMovieDetails;
import in.crazyfingers.starwarschallenge.network.API;
import in.crazyfingers.starwarschallenge.network.MovieInfoPOJO;
import in.crazyfingers.starwarschallenge.network.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    Adapter adapter;
    @BindView(R.id.coordinate)
    CoordinatorLayout mCoordinatorLayout;
    ProgressDialog mProgressDialog;
    ArrayList<MovieInfoPOJO> movieList = new ArrayList();
    private static final String TAG = "MainActivity";
    @BindView(R.id.activity_main_rv_movies)
    RecyclerView rvMovies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        movieList.clear();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Fetching Data......");
        mProgressDialog.setCancelable(false);

        ButterKnife.bind(this);

        getMovies();


    }


    private void getMovies() {

        if (Util.isNetworkAvailable(this)) {
            mProgressDialog.show();
            API apis = RetrofitClient.getApiClient(Contants.DOMAIN_NAME).create(API.class);

            Call<JsonElement> call = apis.getAllFilms(Contants.GET_URL);

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {


                    if (Contants.IS_DEBUG) {
                        Log.d(TAG, "onResponse:  status " + response.code());


                    }

                    if (response.code() == 200) {

                        try {
                            JSONObject obj = new JSONObject(response.body().toString());

                            if (Contants.IS_DEBUG) {
                                Log.d(TAG, "onResponse: " + obj.toString());


                                JSONArray moviesJArray = obj.getJSONArray("results");


                                for (int i = 0; i < moviesJArray.length(); i++) {


                                    MovieInfoPOJO pojo = new MovieInfoPOJO();

                                    JSONObject movieObj = moviesJArray.getJSONObject(i);
                                    JSONArray characters = moviesJArray.getJSONObject(i).getJSONArray("characters");

                                    if (Contants.IS_DEBUG) {
                                        Log.d(TAG, "onResponse: " + characters.toString());
                                    }
                                    pojo.setTitle(movieObj.getString("title"));
                                    pojo.setRelease_date(movieObj.getString("release_date"));
                                    pojo.setProducer(movieObj.getString("producer"));
                                    pojo.setDirector(movieObj.getString("director"));
                                    pojo.setOpening_crawl(movieObj.getString("opening_crawl"));
                                    pojo.setCharacters(characters);

                                    movieList.add(pojo);


                                }

                                if (Contants.IS_DEBUG) {
                                    Log.d(TAG, "onResponse: total movies " + movieList.size());
                                }
                                adapter = new Adapter(movieList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                rvMovies.setLayoutManager(mLayoutManager);
                                rvMovies.setItemAnimator(new DefaultItemAnimator());
                                rvMovies.setAdapter(adapter);

                                rvMovies.addOnItemTouchListener(
                                        new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(View view, int position) {
                                                // TODO Handle item click

                                                switch (position) {

                                                    case 0:


                                                        showDialog("https://www.youtube.com/watch?v=1g3_CFmnU7k", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());

                                                        break;

                                                    case 1:

                                                        showDialog("https://www.youtube.com/watch?v=gYbW1F_c9eM", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());

                                                        break;

                                                    case 2:
                                                        showDialog("https://www.youtube.com/watch?v=bD7bpG-zDJQ", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());

                                                        break;
                                                    case 3:
                                                        showDialog("https://www.youtube.com/watch?v=0xAZN_RM5ks", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());

                                                        break;
                                                    case 4:
                                                        showDialog("https://www.youtube.com/watch?v=MYD_xxY5wEI", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());


                                                        break;
                                                    case 5:
                                                        showDialog("https://www.youtube.com/watch?v=MYD_xxY5wEI", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());

                                                        break;

                                                    case 6:
                                                        showDialog("https://www.youtube.com/watch?v=sGbxmsDFVnE", movieList.get(position).getOpening_crawl(), movieList.get(position).getTitle(),movieList.get(position).getCharacters());

                                                        break;


                                                }
                                            }
                                        })
                                );


                                if (mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                        }
                    }


                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    t.printStackTrace();
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                }
            });

        } else {

            Snackbar snackbar = Snackbar
                    .make(mCoordinatorLayout, "You are not connected to internet", Snackbar.LENGTH_LONG);

            snackbar.show();
        }


    }


    private void showDialog(final String strLink, final String description, final String strMsg, final JSONArray jsonArray) {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(strMsg);

        alertDialogBuilder.setPositiveButton("Movie Details", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                Intent intent = new Intent(MainActivity.this, ActivityMovieDetails.class);
                intent.putExtra("description", description);
                intent.putExtra("charArray",jsonArray.toString()) ;
                intent.putExtra("title",strMsg);
                startActivity(intent);
                arg0.dismiss();

            }
        });

        alertDialogBuilder.setNegativeButton("Watch Trailer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strLink));
                intent.putExtra("force_fullscreen", true);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
