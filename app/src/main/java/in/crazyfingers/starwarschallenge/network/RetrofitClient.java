package in.crazyfingers.starwarschallenge.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by akshayshahane on 23/10/16.
 */

public class RetrofitClient {

    private RetrofitClient() {
    }


    public static Retrofit getApiClient(String baseHost) {


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseHost)
                .build();


        return retrofit;
    }
}
