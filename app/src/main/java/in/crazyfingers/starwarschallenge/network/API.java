package in.crazyfingers.starwarschallenge.network;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by akshayshahane on 23/10/16.
 */

public interface API {

    @GET
    Call<JsonElement> getAllFilms(@Url String url);
}
