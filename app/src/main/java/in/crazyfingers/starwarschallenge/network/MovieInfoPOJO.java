package in.crazyfingers.starwarschallenge.network;

import org.json.JSONArray;

/**
 * Created by akshayshahane on 23/10/16.
 */

public class MovieInfoPOJO {
    String title ,director,producer,opening_crawl,release_date;
    JSONArray characters;

    public JSONArray getCharacters() {
        return characters;
    }

    public void setCharacters(JSONArray characters) {
        this.characters = characters;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getOpening_crawl() {
        return opening_crawl;
    }

    public void setOpening_crawl(String opening_crawl) {
        this.opening_crawl = opening_crawl;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
}
