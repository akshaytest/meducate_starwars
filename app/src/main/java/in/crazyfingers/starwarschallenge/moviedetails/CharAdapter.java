package in.crazyfingers.starwarschallenge.moviedetails;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.crazyfingers.starwarschallenge.R;

/**
 * Created by akshayshahane on 24/10/16.
 */
public class CharAdapter extends RecyclerView.Adapter<CharAdapter.MyViewHolderChar>  {

        private List<CharacterDetailsPOJO> moviesList = new ArrayList<CharacterDetailsPOJO>();




        public class MyViewHolderChar extends RecyclerView.ViewHolder  {


            @BindView(R.id.textView_game_name)
            TextView tvName;

            @BindView(R.id.textView_date)
            TextView tvHeight;

            @BindView(R.id.textView_producer)
            TextView tvGender;


            @BindView(R.id.textView_director)
            TextView tvBirthYear;


            public MyViewHolderChar(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }

        public CharAdapter(List<CharacterDetailsPOJO> moviesList) {
            this.moviesList = moviesList;
        }

        @Override
        public MyViewHolderChar onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_row_char, parent, false);

            return new MyViewHolderChar(itemView);
        }



    @Override
        public void onBindViewHolder(MyViewHolderChar holder, int position) {
        CharacterDetailsPOJO charpojo = moviesList.get(position);
            holder.tvName.setText(charpojo.getName());
            holder.tvHeight.setText(charpojo.getHeight());
            holder.tvGender.setText(charpojo.getGender());
            holder.tvBirthYear.setText(charpojo.getBirth_year());

        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }

    }


