package in.crazyfingers.starwarschallenge.moviedetails;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.crazyfingers.starwarschallenge.R;
import in.crazyfingers.starwarschallenge.constants.Contants;
import in.crazyfingers.starwarschallenge.constants.Util;
import in.crazyfingers.starwarschallenge.network.API;
import in.crazyfingers.starwarschallenge.network.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by akshayshahane on 24/10/16.
 */

public class ActivityMovieDetails extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    ArrayList<CharacterDetailsPOJO> charList = new ArrayList();

    @BindView(R.id.activity_movies_tv_description)
    TextView tvDescription;

    @BindView(R.id.coordinate)
    CoordinatorLayout mCoordinatorLayout;


    @BindView(R.id.activity_movies_rv_characters)
    RecyclerView rvChar;

    CharAdapter adapter ;
    JSONArray array;
    private static final String TAG = "ActivityMovieDetails";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        charList.clear();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Fetching Data......");
        mProgressDialog.setCancelable(false);

        ButterKnife.bind(this);
        Intent intent = getIntent();
        String strDescription = intent.getStringExtra("description");
        String strTitle = intent.getStringExtra("title");
        getSupportActionBar().setTitle(strTitle);
        String jsonArray = intent.getStringExtra("charArray");

        try {
           array = new JSONArray(jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        tvDescription.setText(strDescription);

     getCharacters();





    }

    private void getCharacters() {
        mProgressDialog.show();

        for (int i = 0; i <array.length() ; i++) {


            if (Util.isNetworkAvailable(getApplicationContext())){



                API apis = RetrofitClient.getApiClient(Contants.DOMAIN_NAME).create(API.class);

                try {
                    Call<JsonElement> call = apis.getAllFilms(array.getString(i));

                    call.enqueue(new Callback<JsonElement>() {
                        @Override
                        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                            if (Contants.IS_DEBUG){

                                Log.d(TAG, "onResponse: "+response.code());

                                if (response.code()==200){

                                    try {
                                        JSONObject obj = new JSONObject(response.body().toString());


                                        CharacterDetailsPOJO pojo = new CharacterDetailsPOJO();

                                        pojo.setName(obj.getString("name"));
                                        pojo.setGender(obj.getString("gender"));
                                        pojo.setHeight(obj.getString("height"));
                                        pojo.setBirth_year(obj.getString("birth_year"));


                                        charList.add(pojo);

                                        adapter = new CharAdapter(charList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                        rvChar.setLayoutManager(mLayoutManager);
                                        rvChar.setItemAnimator(new DefaultItemAnimator());
                                        rvChar.setAdapter(adapter);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        if (mProgressDialog.isShowing()){
                                            mProgressDialog.dismiss();
                                        }
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonElement> call, Throwable t) {
                            t.printStackTrace();


                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                    }
                }


            }else {

                Snackbar snackbar = Snackbar
                        .make(mCoordinatorLayout, "You are not connected to internet", Snackbar.LENGTH_LONG);

                snackbar.show();
            }




        }

        Log.d(TAG, "getCharacters: "+charList.size());
        if (mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }

    }
}
