package in.crazyfingers.starwarschallenge.constants;

/**
 * Created by akshayshahane on 24/10/16.
 */

public class Contants {

    public static final String DOMAIN_NAME = "http://swapi.co/api/";
    public static final String GET_URL = "http://swapi.co/api/films/";
    public static final boolean IS_DEBUG = true;
}
