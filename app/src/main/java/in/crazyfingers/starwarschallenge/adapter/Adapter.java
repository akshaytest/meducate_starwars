package in.crazyfingers.starwarschallenge.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.crazyfingers.starwarschallenge.R;
import in.crazyfingers.starwarschallenge.network.MovieInfoPOJO;

/**
 * Created by akshayshahane on 23/10/16.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder>  {

    private List<MovieInfoPOJO> moviesList = new ArrayList<MovieInfoPOJO>();




    public class MyViewHolder extends RecyclerView.ViewHolder  {


        @BindView(R.id.textView_game_name)
        TextView tvMovieName;

        @BindView(R.id.textView_date)
        TextView tvReleaseDate;

        @BindView(R.id.textView_producer)
        TextView tvProducer;


        @BindView(R.id.textView_director)
        TextView tvDirector;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public Adapter(List<MovieInfoPOJO> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MovieInfoPOJO moviepojo = moviesList.get(position);
        holder.tvMovieName.setText(moviepojo.getTitle());
        holder.tvReleaseDate.setText(moviepojo.getRelease_date());
        holder.tvProducer.setText(moviepojo.getProducer());
        holder.tvDirector.setText(moviepojo.getDirector());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
